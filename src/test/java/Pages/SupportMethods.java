package Pages;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SupportMethods {
    private WebDriver webDriver;
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;
    List<WebElement> elements;

    String correctEmail = "pavel.bobrov@startmatter.com";
    String correctPass = "1234";
    String incorrectEmail = "qwe@mail.ru";
    String incorrectPass = "qwe";
    final int max = 256;
    public SupportMethods (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }



    public List<WebElement> getColums() {
        elements = new ArrayList<>();
        webSite = new WebSite(webDriver);
        elements.add(webSite.mainPage().Colum1);
        elements.add(webSite.mainPage().Colum2);
        elements.add(webSite.mainPage().Colum3);
        elements.add(webSite.mainPage().Colum4);
        elements.add(webSite.mainPage().Colum5);
        elements.add(webSite.mainPage().Colum6);
        elements.add(webSite.mainPage().Colum7);
        elements.add(webSite.mainPage().Colum8);
        elements.add(webSite.mainPage().Colum9);
        elements.add(webSite.mainPage().Colum10);
        elements.add(webSite.mainPage().Colum11);
        elements.add(webSite.mainPage().Colum12);
        elements.add(webSite.mainPage().Colum13);
        elements.add(webSite.mainPage().Colum14);
        elements.add(webSite.mainPage().Colum15);
        elements.add(webSite.mainPage().Colum16);
        return elements;
    }

    public List<WebElement> getHeaders() {
        elements = new ArrayList<>();
        webSite = new WebSite(webDriver);
        elements.add(webSite.mainPage().SimpleLeft);
        elements.add(webSite.mainPage().SimpleStacked);
        elements.add(webSite.mainPage().SimpleRight);
        elements.add(webSite.mainPage().SimpleCenter);
        elements.add(webSite.mainPage().CTAStacked);
        return elements;
    }

    public List<WebElement> getBaners() {
        webSite = new WebSite(webDriver);
        List<WebElement> banners= new ArrayList<>();
        banners.add(webSite.mainPage().Banner1);
        banners.add(webSite.mainPage().Banner2);
        banners.add(webSite.mainPage().Banner3);
        banners.add(webSite.mainPage().Banner4);
        banners.add(webSite.mainPage().Banner5);
        banners.add(webSite.mainPage().Banner6);
        banners.add(webSite.mainPage().Banner7);
        banners.add(webSite.mainPage().Banner8);
        banners.add(webSite.mainPage().Banner9);
        banners.add(webSite.mainPage().Banner10);
        banners.add(webSite.mainPage().Banner11);
        banners.add(webSite.mainPage().Banner12);
        return banners;
    }
    public static String rnd(int max)
    {
        int i = (int) (Math.random() * ++max);
        String sRnd = Integer.toString(i);
        return sRnd;
    }


    public List<WebElement> getFontColors() {
        webSite = new WebSite(webDriver);
        List<WebElement> colors= new ArrayList<>();
        colors.add(webSite.sideBar().Color1);
        colors.add(webSite.sideBar().Color2);
        colors.add(webSite.sideBar().Color3);
        colors.add(webSite.sideBar().Color4);
        colors.add(webSite.sideBar().Color5);
        colors.add(webSite.sideBar().Color6);
        colors.add(webSite.sideBar().Color7);
        return colors;
    }
    public List<WebElement> getSectionColors() {
        webSite = new WebSite(webDriver);
        List<WebElement> colors= new ArrayList<>();
        colors.add(webSite.sideBar().HeaderSectionColor1);
        colors.add(webSite.sideBar().HeaderSectionColor2);
        colors.add(webSite.sideBar().HeaderSectionColor3);
        colors.add(webSite.sideBar().HeaderSectionColor4);
        colors.add(webSite.sideBar().HeaderSectionColor5);
        colors.add(webSite.sideBar().HeaderSectionColor6);
        colors.add(webSite.sideBar().HeaderSectionColor7);
        return colors;
    }

    public List getAssrtForFontColor(){
        webSite = new WebSite(webDriver);
        List<String> asserts= new ArrayList<>();
        asserts.add("rgba(105, 210, 231, 1)");
        asserts.add("rgba(167, 219, 216, 1)");
        asserts.add("rgba(224, 228, 204, 1)");
        asserts.add("rgba(243, 134, 48, 1)");
        asserts.add("rgba(250, 105, 0, 1)");
        asserts.add("rgba(255, 255, 255, 1)");
        asserts.add("rgba(0, 0, 0, 1)");
        return asserts;
    }

    public void deleteAllSections(){
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 30, 300);
        List<WebElement> sections = new ArrayList<>();
        sections = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));
        for(int i = 0; i < sections.size(); i++) {
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection));
            builder.moveToElement(webSite.mainPage().ClickBySection).perform();
            builder.moveToElement(webSite.mainPage().ClickBySection).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Delete)).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='delete']")));
            webSite.mainPage().DeleteButton.click();
        }
    }
    public void openPreview(){
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 10, 300);
        List<WebElement> list = new ArrayList<>();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        webSite.mainPage().PagesAndPosts.click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(1).click();
        webSite.sideBar().PreviewPage.click();
    }

    public void deleteAll(){
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 10, 300);
        List<WebElement> list = new ArrayList<>();
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));
        if(list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection));
                webSite.mainPage().ClickBySection.click();
                wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Delete));
                webSite.mainPage().Delete.click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='delete']")));
                webSite.mainPage().DeleteButton.click();
            }
        }
    }
    public void changeCustomColorForms(String R, String G, String B){
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 30, 300);
        List<WebElement> colors = new ArrayList<>();
        colors = webDriver.findElements(By.xpath("//html//div[@class='flex-fit-wrap']/div[2]//input"));
        colors.get(0).sendKeys(Keys.BACK_SPACE);
        colors.get(0).sendKeys(Keys.BACK_SPACE);
        colors.get(0).sendKeys(Keys.BACK_SPACE);
        colors.get(0).sendKeys(R);
        colors.get(1).sendKeys(Keys.BACK_SPACE);
        colors.get(1).sendKeys(Keys.BACK_SPACE);
        colors.get(1).sendKeys(Keys.BACK_SPACE);
        colors.get(1).sendKeys(G);
        colors.get(2).sendKeys(Keys.BACK_SPACE);
        colors.get(2).sendKeys(Keys.BACK_SPACE);
        colors.get(2).sendKeys(Keys.BACK_SPACE);
        colors.get(2).sendKeys(B);
    }

    public void transporency(String A){
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 30, 300);
        List<WebElement> colors = new ArrayList<>();
        colors = webDriver.findElements(By.xpath("//html//div[@class='flex-fit-wrap']/div[2]//input"));
        colors.get(3).sendKeys(Keys.BACK_SPACE);
        colors.get(3).sendKeys(A);
    }

    public void createSimpleLeft() throws InterruptedException {
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 60, 300);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreatePrebuiltsSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().SimpleLeft)).click();
    }

    public void refresh() throws InterruptedException {
        webSite = new WebSite(webDriver);
        Thread.sleep(5000);
        webDriver.navigate().refresh();
        wait = new WebDriverWait(webDriver, 60, 300);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
    }
    public void signIn(){
        webSite = new WebSite(webDriver);
        wait = new WebDriverWait(webDriver, 60, 300);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.loginPage().EmailField)).sendKeys(correctEmail);
        webSite.loginPage().PasswordField.sendKeys(correctPass);
        webSite.loginPage().SignInButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
    }
    public void cleaner() throws InterruptedException {
        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 30, 300);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PagesAndPosts)).click();
        List<WebElement> pages = webDriver.findElements(By.xpath("//div[@class='animationSidebar']//div[@class='pageMenu__item']//span[@class='pageMenu__date']"));
        for(int i = 0; i <= pages.size(); i++){
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PagesAndPosts));
            webSite.mainPage().PagesAndPosts.click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
            webSite.mainPage().ThreeDots.click();
            webSite.mainPage().DeleteElements.click();
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
        }
    }

    public void createBlankAndAddCustom(WebElement element){
        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 30, 300);
        builder = new Actions(webDriver);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Colum1)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PlusInsideSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(element));
        builder.moveToElement(element).perform();
        builder.moveToElement(element).click().perform();
    }

    public void BasicElements(WebElement element) throws InterruptedException {
        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 30, 300);
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        List <WebElement> list = new ArrayList<>();
        List <WebElement> elements = new ArrayList<>();
        elements.add(webSite.mainPage().ElementHeader);
        elements.add(webSite.mainPage().ElementParagraph);
        elements.add(webSite.mainPage().ElementImage);
        elements.add(webSite.mainPage().ElementVideo);
        elements.add(webSite.mainPage().ElementDivider);
        elements.add(webSite.mainPage().ElementButton);
        elements.add(webSite.mainPage().ElementMap);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection));
        webSite.mainPage().CreateCustomSection.click();
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();

        for(int i = 0; i < 6; i++){
            builder.moveToElement(webSite.mainPage().Plus).perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus));
            builder.moveToElement(webSite.mainPage().Plus).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom));
            webSite.mainPage().AddCustom.click();
            builder.moveToElement(element).perform();
            builder.moveToElement(element).click().perform();
        }
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));
        for(int i = 0; i < elements.size();i++){
            builder.moveToElement(list.get(i)).perform();
            builder.moveToElement(list.get(i)).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PlusInsideSection)).click();
            builder.moveToElement(elements.get(i)).perform();
            builder.perform();
            wait.until(ExpectedConditions.elementToBeClickable(elements.get(i))).click();
        }
        Thread.sleep(4000);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections));
        webSite.mainPage().Sections.click();
        webSite.mainPage().PagesAndPosts.click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(0).click();
        webSite.sideBar().PreviewPage.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_headline')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_text')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_image')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_video')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_divider')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_button')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_map')]")).isDisplayed());
    }

    public void Formelements(WebElement element) throws InterruptedException {
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 30, 300);
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        List <WebElement> list = new ArrayList<>();
        List <WebElement> elements = new ArrayList<>();
        elements.add(webSite.mainPage().ElementInput);
        elements.add(webSite.mainPage().ElementTextArea);
        elements.add(webSite.mainPage().ElementDroplist);
        elements.add(webSite.mainPage().ElementCheckbox);
        elements.add(webSite.mainPage().ElementRadiobox);
        elements.add(webSite.mainPage().ElementSubmitButton);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection)).click();
        builder.moveToElement(element).perform();
        builder.moveToElement(element).click().perform();

        for(int i = 0; i < 6; i++){
            builder.moveToElement(webSite.mainPage().Plus).perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus));
            builder.moveToElement(webSite.mainPage().Plus).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom));
            webSite.mainPage().AddCustom.click();
            builder.moveToElement(element).perform();
            builder.moveToElement(element).click().perform();
        }
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));

        for(int i = 0; i < elements.size();i++){
            builder.moveToElement(list.get(i)).perform();
            builder.moveToElement(list.get(i)).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PlusInsideSection)).click();
            builder.moveToElement(elements.get(i)).perform();
            builder.perform();
            wait.until(ExpectedConditions.elementToBeClickable(elements.get(i))).click();
        }
        Thread.sleep(4000);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        webSite.mainPage().PagesAndPosts.click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(0).click();
        webSite.sideBar().PreviewPage.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_input')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_text')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_droplist')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_checkbox')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_radiobox')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_button')]")).isDisplayed());
    }

    public void AdvancedElements(WebElement element) throws InterruptedException {
        builder = new Actions(webDriver);
        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 30, 300);
        List<WebElement> elements = new ArrayList<>();
        elements.add(webSite.mainPage().ElementTimer);
        elements.add(webSite.mainPage().ElementSocial);
        elements.add(webSite.mainPage().ElementHTML);
        elements.add(webSite.mainPage().ElementComments);
        elements.add(webSite.mainPage().ElementNavigation);


        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        List<WebElement> list = new ArrayList<>();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection));
        webSite.mainPage().CreateCustomSection.click();
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();

        for (int i = 0; i < 5; i++) {
            builder.moveToElement(webSite.mainPage().Plus).perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus));
            builder.moveToElement(webSite.mainPage().Plus).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom)).click();
            builder.moveToElement(element).perform();
            builder.moveToElement(element).click().perform();
        }
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));

        for (int i = 0; i < elements.size(); i++) {
            builder.moveToElement(list.get(i)).perform();
            builder.moveToElement(list.get(i)).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PlusInsideSection)).click();
            builder.moveToElement(elements.get(i)).perform();
            builder.perform();
            wait.until(ExpectedConditions.elementToBeClickable(elements.get(i))).click();
        }
        Thread.sleep(4000);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        webSite.mainPage().PagesAndPosts.click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(0).click();
        webSite.sideBar().PreviewPage.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@id,'tmp_comment')]")));
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'countdown-tmp_countdown')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_social')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_comment')]")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'tmp_nav')]")).isDisplayed());
    }
}
