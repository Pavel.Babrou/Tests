package Pages;
import io.github.bonigarcia.wdm.OperaDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaDriverService;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestsColumsFilling {
    WebDriver webDriver;
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
//        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void preCondition() throws IOException {
//        webDriver = new ChromeDriver();
//        webDriver = new FirefoxDriver();

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1200x600");
        webDriver = new ChromeDriver(chromeOptions);

        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 90, 300);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
        webDriver.get("https://app.heroicnow.com/");
    }

    /*********** ЭЛЕМЕНТЫ ДЛЯ КАСТОМНЫХ СЕКЦИЙ ***********/


    @Test
    public void BasicCol1() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum1);
    }
    @Test
    public void FormCol1() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum1);
    }
    @Test
    public void AdcCol1() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum1);
    }

    @Test
    public void BasicCol2() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum2);
    }
    @Test
    public void FormCol2() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum2);
    }
    @Test
    public void AdcCol2() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum2);
    }
    @Test
    public void BasicCol3() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum3);
    }
    @Test
    public void FormCol3() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum3);
    }
    @Test
    public void AdcCol3() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum3);
    }
    @Test
    public void BasicCol4() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum4);
    }
    @Test
    public void FormCol4() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum4);
    }
    @Test
    public void AdcCol4() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum4);
    }
    @Test
    public void BasicCol5() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum5);
    }
    @Test
    public void FormCol5() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum5);
    }
    @Test
    public void AdcCol5() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum5);
    }
    @Test
    public void BasicCol6() throws InterruptedException {
        webSite.supportMethods().BasicElements(webSite.mainPage().Colum6);
    }
    @Test
    public void FormCol6() throws InterruptedException {
        webSite.supportMethods().Formelements(webSite.mainPage().Colum6);
    }
    @Test
    public void AdcCol6() throws InterruptedException {
        webSite.supportMethods().AdvancedElements(webSite.mainPage().Colum6);
    }

    @After
    public void postCondition() {
        if(webDriver != null)
            webDriver.quit();
    }
}
