package Pages;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestsMoveElements {
    WebDriver webDriver;
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
//        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void preCondition(){
        //        webDriver = new ChromeDriver();
//        webDriver = new FirefoxDriver();

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1200x600");
        webDriver = new ChromeDriver(chromeOptions);

        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 90, 300);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
        webDriver.get("https://app.heroicnow.com/");
    }


    @Test
    public void DeleteNavigation() throws InterruptedException {
        List<WebElement> sections = new ArrayList<>();
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();

        builder.moveToElement(webSite.mainPage().AddNewPage).perform();
        builder.moveToElement(webSite.mainPage().AddNewPage).click().perform();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();

        //Добавляем все секции на пустую страницу
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.supportMethods().getColums().get(0))).click();
        for (int i = 1; i < webSite.supportMethods().getColums().size(); i++) {
            builder.moveToElement(webSite.mainPage().Plus).perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus));
            builder.moveToElement(webSite.mainPage().Plus).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom));
            webSite.mainPage().AddCustom.click();
            builder.moveToElement(webSite.supportMethods().getColums().get(i)).perform();
            builder.moveToElement(webSite.supportMethods().getColums().get(i)).click().perform();
        }
        Thread.sleep(4000);
        Assert.assertTrue(webSite.mainPage().Sections.isDisplayed());
    }

    @Test
    public void changeSectionsPosition() throws InterruptedException {
        builder = new Actions(webDriver);

        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        builder.moveToElement(webSite.mainPage().AddNewPage).perform();
        builder.moveToElement(webSite.mainPage().AddNewPage).click().perform();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Colum1)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom)).click();
        builder.moveToElement(webSite.mainPage().Colum5).perform();
        builder.moveToElement(webSite.mainPage().Colum5).click().perform();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddCustom)).click();
        builder.moveToElement(webSite.mainPage().Colum1).perform();
        builder.moveToElement(webSite.mainPage().Colum1).click().perform();
        List<WebElement> list = new ArrayList<>();
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(0))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().MoveDown)).click();
        wait.until(ExpectedConditions.elementToBeClickable( list.get(2))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().MoveUp)).click();
        list = webDriver.findElements(By.xpath("//div[@class='content-sections']//div[contains(@id,'section')]"));

        Assert.assertTrue(list.get(0).getAttribute("class").contains("section-index-0"));
        Assert.assertTrue(list.get(1).getAttribute("class").contains("section-index-1"));
        Assert.assertTrue(list.get(2).getAttribute("class").contains("section-index-2"));
    }


    @After
    public void postCondition() throws InterruptedException {
        if(webDriver != null)
            webDriver.quit();
    }
}
