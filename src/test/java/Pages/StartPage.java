package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StartPage {
    private WebDriver webDriver;

    public StartPage (WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy (xpath = "//a[@class='header-login']")
    WebElement Login;
    @FindBy (css = "body > div.header > div.header-info > div > a")
    WebElement GetStartedButton;
    @FindBy (xpath = "//html//div[@class='page-category__sites-list sites-list']/div[1]/div[1]")
    WebElement SmartStart;
    @FindBy (xpath = "//html//div[1]/div[1]/div[3]/a[1]")
    WebElement PreviewSmartStart;
    @FindBy (css = "div.page-preview div.page-preview__container.mode-slim div.page-preview__content div.category-description-wrap div.slim-mode-button > a.btn-default.btn-default_sm")
    WebElement StartWithSmartStart;

}
