package Pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.UnableToCreateProfileException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestsChangeColorAndFontSize {
    WebDriver webDriver;
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
//        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void preCondition() {
        //        webDriver = new ChromeDriver();
//        webDriver = new FirefoxDriver();

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1200x600");
        webDriver = new ChromeDriver(chromeOptions);

        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 90, 300);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
        webDriver.get("https://app.heroicnow.com/");
    }
    @Test
    public void ChangeFontColor() throws InterruptedException {
        webSite.supportMethods().signIn();
        builder = new Actions(webDriver);
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        builder.doubleClick(webSite.sideBar().NavItem).perform();
        builder.moveToElement(webSite.sideBar().AlignLeft).perform();
        for(int i = 0; i < webSite.supportMethods().getFontColors().size();i++){
            webSite.supportMethods().getFontColors().get(i).click();
            Assert.assertTrue(webSite.sideBar().NavItem.getCssValue("color").contains((CharSequence)webSite.supportMethods().getAssrtForFontColor().get(i)));
        }
        Assert.assertTrue(webSite.sideBar().NavItem.getCssValue("color").contains("rgba(0, 0, 0, 1)"));
        webSite.supportMethods().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(webSite.sideBar().NavItem.getCssValue("color").contains("rgba(0, 0, 0, 1)"));
    }

    @Test
    public void changeFontCustomColor() throws InterruptedException {
        String str1;
        String str2;
        String str3;
        webSite.supportMethods().signIn();
        builder = new Actions(webDriver);
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        List<WebElement> colors = new ArrayList<>();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        builder.doubleClick(webSite.sideBar().NavItem).perform();
        builder.moveToElement(webSite.sideBar().AlignLeft).perform();
        builder.moveToElement(webSite.sideBar().FontColorDropDown).perform();
        builder.moveToElement(webSite.sideBar().FontColorDropDown).click().perform();
        colors = webDriver.findElements(By.xpath("//html//div[@class='flex-fit-wrap']/div[2]//input"));
        builder.moveToElement(colors.get(1)).perform();
        for(int i = 0; i < 5;i++){
            str1 = webSite.supportMethods().rnd(255);
            str2 = webSite.supportMethods().rnd(255);
            str3 = webSite.supportMethods().rnd(255);
            webSite.supportMethods().changeCustomColorForms(str1, str2, str3);
            Thread.sleep(2000);
            Assert.assertTrue(webSite.mainPage().NavFontColor.getCssValue("color").contains("rgba("+str1+", "+str2+", "+ str3 +", 1)"));
        }
        webSite.supportMethods().changeCustomColorForms("100", "23", "200");
        builder.moveToElement(webSite.sideBar().Save).perform();
        builder.moveToElement(webSite.sideBar().Save).click().perform();
        Assert.assertTrue(webSite.sideBar().NavItem.getCssValue("color").contains("rgba(100, 23, 200, 1)"));
        webSite.supportMethods().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(webSite.sideBar().NavItem.getCssValue("color").contains("rgba(100, 23, 200, 1)"));
    }
    @Test
    public void changeColumnColor() throws InterruptedException {
        webSite.supportMethods().signIn();
        builder = new Actions(webDriver);
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        builder.doubleClick(webSite.sideBar().NavItem).perform();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ColumnSetup)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ColumnColor1)).click();
        String CSScolor1 = webSite.mainPage().ColumnColor.getCssValue("background-color");
        System.out.println("THIS IS Color " + CSScolor1);
        Assert.assertTrue(webSite.mainPage().ColumnColor.getCssValue("background-color").contains("rgba(105, 210, 231, 1)"));
        webSite.supportMethods().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(webSite.mainPage().ColumnColor.getCssValue("background-color").contains("rgba(105, 210, 231, 1)"));
    }
    @Test
    public void changeCustomColumnColor() throws InterruptedException {
        webSite.supportMethods().signIn();
        builder = new Actions(webDriver);
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        List<WebElement> colors = new ArrayList<>();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        builder.doubleClick(webSite.sideBar().NavItem).perform();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ColumnSetup)).click();
        colors = webDriver.findElements(By.xpath("//html//div[@class='colorPicker']//div[@class='drop-color-picker-wrap']"));
        builder.moveToElement(colors.get(0)).perform();
        builder.moveToElement(colors.get(0)).click().perform();
        colors = webDriver.findElements(By.xpath("//html//div[@class='flex-fit-wrap']/div[2]//input"));
        builder.moveToElement(colors.get(1)).perform();
        webSite.supportMethods().changeCustomColorForms("200","1","100");
        builder.moveToElement(webSite.sideBar().Save).perform();
        builder.moveToElement(webSite.sideBar().Save).click().perform();
        Assert.assertTrue(webSite.mainPage().ColumnColor.getCssValue("background-color").contains("rgba(200, 1, 100, 1)"));
        webSite.supportMethods().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(webSite.mainPage().ColumnColor.getCssValue("background-color").contains("rgba(200, 1, 100, 1)"));
    }

    @Test
    public void changeSectionColor() throws InterruptedException {
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionSetup)).click();
        for(int i = 0; i < webSite.supportMethods().getSectionColors().size();i++){
            webSite.supportMethods().getSectionColors().get(i).click();
            Assert.assertTrue(webSite.mainPage().Section.getCssValue("background-color").contains((CharSequence)webSite.supportMethods().getAssrtForFontColor().get(i)));
        }
        webSite.supportMethods().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(webSite.mainPage().Section.getCssValue("background-color").contains("rgba(0, 0, 0, 1)"));

    }

    @Test
    public void changeSectionCustomColor() throws InterruptedException {
        String str1;
        String str2;
        String str3;
        List<WebElement> list = new ArrayList<>();
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionSetup)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionCustomColor)).click();
        for(int i = 0; i < 5;i++){
            str1 = webSite.supportMethods().rnd(255);
            str2 = webSite.supportMethods().rnd(255);
            str3 = webSite.supportMethods().rnd(255);
            webSite.supportMethods().changeCustomColorForms(str1, str2, str3);
            Thread.sleep(2000);
            Assert.assertTrue(webSite.mainPage().Section.getCssValue("background-color").contains("rgba("+str1+", "+str2+", "+ str3 +", 1)"));
        }
        webSite.supportMethods().changeCustomColorForms("100", "23", "200");
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Save)).click();
        Assert.assertTrue(webSite.mainPage().Section.getCssValue("background-color").contains("rgba(100, 23, 200, 1)"));
        Thread.sleep(4000);
        webSite.supportMethods().refresh();
        list = webDriver.findElements(By.xpath("//div[@class='animationSidebar']//div[@class='pageMenu__item']//span[@class='pageMenu__date']"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(0))).click();
        Assert.assertTrue(webSite.mainPage().Section.getCssValue("background-color").contains("rgba(100, 23, 200, 1)"));
    }

    @Test
    public void changelogoColor() throws InterruptedException {
        List<WebElement> list = new ArrayList<>();
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        list = webDriver.findElements(By.xpath("//div[contains(@class,'column-col')]"));
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ColumnSetup)).click();
        for(int i = 0; i < webSite.supportMethods().getSectionColors().size();i++){
            webSite.supportMethods().getSectionColors().get(i).click();
            Assert.assertTrue(list.get(1).getCssValue("background-color").contains((CharSequence)webSite.supportMethods().getAssrtForFontColor().get(i)));
        }
        System.out.println(list.get(1).getCssValue("background-color"));
        webSite.supportMethods().refresh();
        list = webDriver.findElements(By.xpath("//div[contains(@class,'column-col')]"));
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        Assert.assertTrue(list.get(1).getCssValue("background-color").contains("rgba(0, 0, 0, 1)"));
    }

    @Test
    public void changeLogoCustomColor() throws InterruptedException {
        String str1;
        String str2;
        String str3;
        List<WebElement> list = new ArrayList<>();
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        list = webDriver.findElements(By.xpath("//div[contains(@class,'column-col')]"));
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ColumnSetup)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionCustomColor)).click();
        for(int i = 0; i < 5;i++){
            str1 = webSite.supportMethods().rnd(255);
            str2 = webSite.supportMethods().rnd(255);
            str3 = webSite.supportMethods().rnd(255);
            webSite.supportMethods().changeCustomColorForms(str1, str2, str3);
            Thread.sleep(2000);
            Assert.assertTrue(list.get(1).getCssValue("background-color").contains("rgba("+str1+", "+str2+", "+ str3 +", 1)"));
        }
        webSite.supportMethods().changeCustomColorForms("100", "23", "200");
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Save)).click();
        Assert.assertTrue(list.get(1).getCssValue("background-color").contains("rgba(100, 23, 200, 1)"));
        Thread.sleep(4000);
        webSite.supportMethods().refresh();
        list = webDriver.findElements(By.xpath("//div[contains(@class,'column-col')]"));
        Assert.assertTrue(list.get(1).getCssValue("background-color").contains("rgba(100, 23, 200, 1)"));
    }

    @Test
    public void changeTransparency() throws InterruptedException {
        List<WebElement> list = new ArrayList<>();
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().deleteAll();
        webSite.supportMethods().createSimpleLeft();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionSetup)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SectionCustomColor)).click();
        list = webDriver.findElements(By.xpath("//html//div[@class='flex-fit-wrap']/div[2]//input"));

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        webSite.supportMethods().changeCustomColorForms("100", "23", "200");
        double i = 0.59;
            String str1 = df.format(i).toString();
            double db = Double.parseDouble(str1.replace( ',', '.' ) );
            String str = Double.toString(db);
            list.get(3).sendKeys(Keys.BACK_SPACE);
            list.get(3).sendKeys(Keys.BACK_SPACE);
            list.get(3).sendKeys(Keys.BACK_SPACE);
            list.get(3).sendKeys(Keys.BACK_SPACE);
            Thread.sleep(300);
            list.get(3).sendKeys(str);
            Assert.assertTrue(webSite.mainPage().SectionColor.getCssValue("background-color").contains("rgba(100, 23, 200, "+str+")"));
    }

    @Test
    public void paragraphFontColor() throws InterruptedException {
        List <WebElement> list = new ArrayList<>();
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().createBlankAndAddCustom(webSite.mainPage().ElementParagraph);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ParagraphSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AjustFontStyle)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ParagraphFontColorDropDown)).click();
        builder.moveToElement(webSite.sideBar().Save).perform();
        webSite.supportMethods().changeCustomColorForms("255", "34", "0");
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Save)).click();
        Thread.sleep(4000);
        webSite.supportMethods().openPreview();
        list = webDriver.findElements(By.xpath("//div[contains(@id,'tmp_text')]"));
        Assert.assertTrue(list.get(1).getCssValue("color").contains("rgba(255, 34, 0, 1)"));
    }
    @Test
    public void paragraphFontSize() throws InterruptedException {
        List <WebElement> list = new ArrayList<>();
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        webSite.supportMethods().createBlankAndAddCustom(webSite.mainPage().ElementParagraph);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ParagraphSection)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AjustFontStyle)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().FontSizeLarge)).click();
        Thread.sleep(5000);
        webSite.supportMethods().openPreview();
        list = webDriver.findElements(By.xpath("//div[contains(@id,'tmp_text')]"));
        System.out.println(list.size());
        System.out.println(list.get(0).getCssValue("font-size"));
        System.out.println(list.get(1).getCssValue("font-size"));
        System.out.println(list.get(2).getCssValue("font-size"));
        System.out.println(list.get(3).getCssValue("font-size"));
        System.out.println(list.get(4).getCssValue("font-size"));
        System.out.println(list.get(5).getCssValue("font-size"));
        Assert.assertTrue(list.get(0).getCssValue("font-size").contains("17px"));
    }






//    @Test
//    public void textAreaFontColor() throws InterruptedException {
//        List <WebElement> colors = new ArrayList<>();
//        builder = new Actions(webDriver);
//        webSite.supportMethods().signIn();
//        webSite.supportMethods().cleaner();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateCustomSection)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Colum1)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PlusInsideSection)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ElementTextArea));
//        builder.moveToElement(webSite.mainPage().ElementTextArea).perform();
//        builder.moveToElement(webSite.mainPage().ElementTextArea).click().perform();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Edit)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().TextAreaSection)).click();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().GlobalLock)).click();
//        builder.moveToElement(webSite.sideBar().InputFieldBorder).perform();
//        builder.moveToElement(webSite.sideBar().InputFieldBorder).click().perform();
//        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().TextAreaFontColorDropDown)).click();
//    }

    @After
    public void postCondition() {
        if(webDriver != null)
            webDriver.quit();
    }
}
