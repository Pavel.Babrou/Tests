package Pages;

public class Variable {
    public static String lessThanMinPass = "1234";
    public static String minPass = "12345";
    public static String maxPass = "012345678901234567891234";
    public static String moreThanMaxPass = "0123456789012345678912345";
    public static String averagePass = "0123456789";

    public static String lessThanMinFirstName = "qw";
    public static String moreThanMaxFirstName = "вЭтомТекстеБолееТридцатиСимволов";
    public static String averageFirstName = "этоСредняяДлина";
    public static String onlySpacesFirstName = "   ";
    public static String specialCharFirstName = "!@#$%&*?:%;№";

    public static String lessThanMinLastName = "qw";
    public static String moreThanMaxLastname = "вЭтомТекстеБолееТридцатиСимволов";
    public static String averageLastName = "этоСредняяДлина";
    public static String onlySpacesLastName = "   ";
    public static String specialCharLastName = "!@#$%&*?:%;№";

    public static String noDomainEmail = "hfnvghds@gmail";
    public static String specialCharInGlobalPartEmail = "hfnvghds@gm@#$ail.com";
    public static String specialCharInLocalPartEmail = "hfn(\")vghds@gmail";
    public static String moreThanMaxGlobalPartEmail = "hgfhdfsf@thistextcontainsaboutsixtyfourcharactersifyouaddthitoitqwertyuio.com";
    public static String averageGlobalPartEmail = "hgfhdfsf@thistextcoifyouaddthitoitqwertyuio.com";
    public static String lotsOfCharInLocalPart = "qwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww" +
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww" +
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww@gmail.com";

}
